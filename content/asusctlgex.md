+++
title = "asusctl-gex"
template = "page/default.html"
+++

## get asusctl-gex

You can get the current version of [asusctl-gex at extensions.gnome.org](https://extensions.gnome.org/extension/4320/asusctl-gex/)!

If you want to contribute, be sure to checkout our [Gitlab repo](https://gitlab.com/asus-linux/asusctl-gex).

## Icons/Screenshots

*The screenshots below are just examples and might not represent the current used icons.*

### [](#power-profile-daemon-icons)Power Profile Daemon icons

| Icon | Profile |
| --- | --- |
| ![](https://gitlab.com/asus-linux/asusctl-gex/-/raw/main/screenshots/panel-silent.svg) | Power Saver |
| ![](https://gitlab.com/asus-linux/asusctl-gex/-/raw/main/screenshots/panel-normal.svg) | Balanced |
| ![](https://gitlab.com/asus-linux/asusctl-gex/-/raw/main/screenshots/panel-boost.svg) | Performance |

### [](#gpu-mode-icons)GPU mode icons

| Icon | Description |
| --- | --- |
| ![Dedicated, discrete graphics](https://gitlab.com/asus-linux/asusctl-gex/-/raw/main/screenshots/panel-dedicated.svg) | Dedicated, discrete graphics (X11 only!) |
| ![integrated](https://gitlab.com/asus-linux/asusctl-gex/-/raw/main/screenshots/panel-integrated.svg) | Integrated |
| ![Integrated, dedicated GPU active](https://gitlab.com/asus-linux/asusctl-gex/-/raw/main/screenshots/panel-integrated-active.svg) | Integrated, dedicated GPU active* |
| ![Compute](https://gitlab.com/asus-linux/asusctl-gex/-/raw/main/screenshots/panel-compute.svg) | Compute |
| ![Compute, dedicated GPU active](https://gitlab.com/asus-linux/asusctl-gex/-/raw/main/screenshots/panel-compute-active.svg) | Compute, dedicated GPU active |
| ![VFIO](https://gitlab.com/asus-linux/asusctl-gex/-/raw/main/screenshots/panel-vfio.svg) | VFIO |
| ![VFIO, dedicated GPU active](https://gitlab.com/asus-linux/asusctl-gex/-/raw/main/screenshots/panel-vfio-active.svg) | VFIO, dedicated GPU active |
| ![Hybrid](https://gitlab.com/asus-linux/asusctl-gex/-/raw/main/screenshots/panel-hybrid.svg) | Hybrid |
| ![Hybrid, dedicated GPU active](https://gitlab.com/asus-linux/asusctl-gex/-/raw/main/screenshots/panel-hybrid-active.svg) | Hybrid, dedicated GPU active |

*\* on integrated the dedicated GPU should never be active. If this is the case it is possible that another application woke it up by rescanning the PCI bus. It's also possible that the NVIDIA drivers or asusctl is not configured properly.*

### [](#screenshots)Screenshots

**Power Profile Daemon change notifications:**

![cpu-change-silent.png](https://gitlab.com/asus-linux/asusctl-gex/-/raw/main/screenshots/cpu-change-silent.png)

**Graphics Mode change notification:**

![](https://gitlab.com/asus-linux/asusctl-gex/-/raw/main/screenshots/gfx-change-hybrid.png)

**Panel (example):**

![panel-normal-compute.png](https://gitlab.com/asus-linux/asusctl-gex/-/raw/main/screenshots/panel-normal-compute.png)

![panel-normal-compute-hover.png](https://gitlab.com/asus-linux/asusctl-gex/-/raw/main/screenshots/panel-normal-compute-hover.png)

![panel-normal-compute-open.png](https://gitlab.com/asus-linux/asusctl-gex/-/raw/main/screenshots/panel-normal-compute-open.png)